VERSION 5.00
Begin VB.Form cryp 
   Caption         =   "Form1"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9870
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   9870
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox resultado 
      Height          =   495
      Left            =   480
      TabIndex        =   2
      Text            =   "Text1"
      Top             =   1560
      Width           =   6975
   End
   Begin VB.TextBox cuc 
      Height          =   375
      Left            =   480
      TabIndex        =   1
      Text            =   "Text2"
      Top             =   960
      Width           =   2175
   End
   Begin VB.TextBox senha 
      Height          =   375
      Left            =   480
      TabIndex        =   0
      Text            =   "Text1"
      Top             =   360
      Width           =   2175
   End
End
Attribute VB_Name = "cryp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cuc_Click()

resultado.Text = Encrypt(senha.Text, cuc.Text)

End Sub

'***************************************************************************************************
'
'  Function Name : EncryptCUC
'
'  Parameters    : password - Senha a ser criptografada
'                  cuc - CUC do cliente do Banco
'
'  Purpose   : Criptografa a senha de 8 caracteres para 64 bytes
'
'  Returns   : Senha criptografada
'
'**************************************************************************************************
Function Encrypt(password, cuc)


 Dim passo1(12), passo2(12)
  Dim passo3(12)
  Dim semente, senha_completa
  Dim tamanho_senha
  Dim ncuc, iloop, senha_inversa, PARC, indice, PARC1, iloop2, resposta, tamanho
  
  ncuc = CStr(cuc) & "5"

  ' processa a senha
  
  tamanho_senha = Len(password)

  senha_completa = ""

  If tamanho_senha < 8 Then
      
     For iloop = 1 To 8 - tamanho_senha
         
       senha_completa = senha_completa & iloop
                           
     Next
         
  End If

  senha_completa = senha_completa & password
  senha_inversa = ""
  
  For iloop = 8 To 1 Step -1
         
      senha_inversa = senha_inversa & Mid(senha_completa, iloop, 1)
                           
  Next

  For iloop = 1 To 40

     semente = semente & senha_completa & senha_inversa

  Next

  For iloop = 1 To 8
    
    PARC = Mid(senha_completa, iloop, 1)
    PARC = Asc(PARC)
        
    indice = PARC
    
    PARC1 = 0
    
    For iloop2 = 1 To 4
      
        PARC = Mid(semente, indice, iloop2)
        PARC1 = PARC1 + Asc(PARC)
                  
    Next
        
    passo1(iloop) = PARC1
        
   Next

  'processa o numero do cuc

  For iloop = 1 To 8
     
      indice = CInt(Mid(ncuc, iloop, 1))
      
      PARC1 = 0
      
      For iloop2 = 1 To 4
      
        PARC = Mid(semente, indice * iloop2 + 1, iloop2)
        PARC1 = PARC1 + Asc(PARC)
                   
      Next
      
      passo2(iloop) = PARC1
   
  Next

  resposta = ""
  
  For iloop = 1 To 8
             
      PARC = passo1(iloop) - passo2(iloop)
      
      passo3(iloop) = CStr(Abs(PARC)) & passo1(iloop)

      tamanho = Len(passo3(iloop))
               
      If tamanho < 8 Then
      
         resposta = resposta & Left(passo3(iloop), 8 - tamanho)
                           
      End If
      
      resposta = resposta & passo3(iloop)
           
  Next

  Encrypt = resposta

End Function

